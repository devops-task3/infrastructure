# A load balancer should be set up for the web server. SSL termination should be 
# configured for all incoming HTTP requests.

# This configuration sets up an Application Load Balancer with SSL termination. 
# Incoming HTTP requests on port 80 will be redirected to HTTPS on port 443. 
# SSL termination will occur at the load balancer using the ACM certificate. 
# The backend web application listens on port 80 for incoming traffic from the load balancer.

resource "aws_security_group" "lb_sg" {
  name        = "Rails_web_server-SG"
  description = "Rails_web_server sg"
  vpc_id      = aws_vpc.main.id

  // Rails web server accepting incoming HTTP requests
  ingress {
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lb" "ecs_alb" {
  name               = "ecs-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.lb_sg.id]
  subnets            = aws_subnet.public_subnets[*].id

  # Logging to AWS CloudWatch (need to create bucket)
  # access_logs {
  #   bucket  = aws_s3_bucket.example_logs.bucket
  #   prefix  = "load-balancer"
  #   enabled = true
  # }

}

resource "aws_lb_target_group" "ecs_tg" {
  name        = "ecs-target-group"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.main.id
  target_type = "ip"

  health_check {
    path = "/"
    # protocol = "HTTP"
    # matcher  = "200-499"
  }
}

resource "aws_lb_listener" "frontend" {
  load_balancer_arn = aws_lb.ecs_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    #   type = "redirect"
    #   redirect {
    #     port        = "443"
    #     protocol    = "HTTPS"
    #     status_code = "HTTP_301"
    #   }
    type             = "forward"
    target_group_arn = aws_lb_target_group.ecs_tg.arn
  }
}

# if need to use https
# resource "aws_lb_listener" "frontend_tls" {
#   load_balancer_arn = aws_lb.frontend_alb.id
#   port              = "443"
#   protocol          = "HTTPS"
#   ssl_policy        = "ELBSecurityPolicy-2016-08"
#   certificate_arn   = "your_certificate_arn"

#   default_action {
#     target_group_arn = aws_lb_target_group.frontend_alb_tg.arn
#     type             = "forward"
#   }
# }
