# Create the IAM instance profile
resource "aws_iam_instance_profile" "example" {
  name = "example-instance-profile"
  role = aws_iam_role.example.name
}

# Set up an AWS IAM role with read and write access to the AWS S3 bucket.
# data "aws_iam_policy_document" "s3_rw_policy" {
#   statement {
#     actions = [
#       "s3:PutObject",
#       "s3:GetObject",
#       "s3:DeleteObject",
#     ]

#     resources = [
#       "${aws_s3_bucket.example.arn}/*"
#     ]
#   }
# }

## Add permission for instances to send logs to CloudWatch
# data "aws_iam_policy_document" "cloudwatch_logs" {
#   statement {
#     actions = [
#       "logs:CreateLogStream",
#       "logs:PutLogEvents",
#       "logs:DescribeLogStreams"
#     ]

#     resources = [aws_cloudwatch_log_group.example.arn]
#   }
# }

# resource "aws_iam_policy" "s3_rw_policy" {
#   name        = "example-s3-rw-access"
#   description = "Read and Write access to the example S3 bucket"
#   policy      = data.aws_iam_policy_document.s3_rw_policy.json
# }

# resource "aws_iam_policy" "cloudwatch_logs" {
#   name        = "example-cloudwatch-logs"
#   description = "Allow sending logs to CloudWatch from the example instances"
#   policy      = data.aws_iam_policy_document.cloudwatch_logs.json
# }

resource "aws_iam_role" "example" {
  name = "example-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

# resource "aws_iam_role_policy_attachment" "example_s3_rw_policy_attachment" {
#   policy_arn = aws_iam_policy.s3_rw_policy.arn
#   role       = aws_iam_role.example.name
# }

# resource "aws_iam_role_policy_attachment" "example_instance_logs_to_cloudwatch" {
#   policy_arn = aws_iam_policy.cloudwatch_logs.arn
#   role       = aws_iam_role.example.name
# }
