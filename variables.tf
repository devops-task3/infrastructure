# URL to PostgreSQL database, assuming it is already set up.
variable "DB_URL" {
  default = "DB_URL"
}

# ELASTICSEARCH_URL - URL to AWS Opensearch instance - to be created during deployment
# REDIS_URL - URL to AWS Elastic Cache Redis - to be created during deployment

# credentials and identifiers for AWS S3 - to be created during deployment

# variable "AWS_REGION" {
#   description = "AWS_REGION"
#   type        = string
# }

# variable "AWS_ACCESS_KEY_ID" {
#   description = "AWS_ACCESS_KEY_ID"
#   type        = string
# }

# variable "AWS_SECRET_ACCESS_KEY" {
#   description = "AWS_SECRET_ACCESS_KEY"
#   type        = string
# }

# variable "BUCKET_NAME" {
#   description = "BUCKET_NAME"
#   type        = string
# }


#### vpc
variable "vpc_cidr" {
  default = "10.0.0.0/22"
}

variable "public_subnets_cidrs" {
  default = [
    "10.0.1.0/24",
    "10.0.2.0/24",
  ]
}

data "aws_availability_zones" "available" {}

variable "env" {
  default = "dev"
}
