# Autoscaling based on CPU and memory usage should be set up for both the web server 
# and the job processor. It should also be possible to reconfigure it later

# get latest ami
data "aws_ami" "latest_amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}


# Launch configuration
# we should use aws_launch_template instead of old aws_launch_configuration

# resource "aws_launch_configuration" "example" {
#   name_prefix   = "asg-"
#   image_id      = data.aws_ami.latest_amazon_linux.id
#   instance_type = "t3.micro"

#   # Optional: Define security group, user data, role, key, etc.
#   security_groups = ["your_security_group_id"]
#   user_data       = "your_user_data"
#   key_name        = "your_key_name"

#   # add role:
#   iam_instance_profile = aws_iam_instance_profile.example.name

#   # Make sure the instances are healthy
#   lifecycle {
#     create_before_destroy = true
#   }

#   # Enable detailed monitoring
#   enable_monitoring = true
# }

resource "aws_launch_template" "ecs_lt" {
  name_prefix   = "ecs-template"
  image_id      = data.aws_ami.latest_amazon_linux.id
  instance_type = "t3.micro"

  key_name               = "ec2ecs-key"
  vpc_security_group_ids = [aws_security_group.security_group.id]
  iam_instance_profile {
    name = aws_iam_instance_profile.example.name
  }

  block_device_mappings {
    device_name = "/dev/xvda"
    ebs {
      volume_size = 30
      volume_type = "gp3"
    }
  }

  tag_specifications {
    resource_type = "instance"
    tags = {
      Name = "ecs-instance"
    }
  }
  user_data = filebase64("${path.module}/ecs.sh")
}

# Auto Scaling Group
resource "aws_autoscaling_group" "ecs_asg" {
  name_prefix         = "example-asg-"
  vpc_zone_identifier = aws_subnet.public_subnets[*].id

  min_size         = 1
  desired_capacity = 1
  max_size         = 3

  # target_group_arns = [aws_lb_target_group.frontend_alb_tg.arn]

  launch_template {
    id      = aws_launch_template.ecs_lt.id
    version = "$Latest"
  }

  # Make sure instances are healthy
  lifecycle {
    create_before_destroy = true
  }
}

# CloudWatch Alarms for CPU usage
resource "aws_cloudwatch_metric_alarm" "cpu_high" {
  alarm_name          = "example-cpu-high"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "SampleCount"
  threshold           = "70"
  alarm_description   = "This metric checks for high CPU usage"
  alarm_actions       = [aws_autoscaling_policy.scale_out.arn]
}

resource "aws_cloudwatch_metric_alarm" "cpu_low" {
  alarm_name          = "example-cpu-low"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "SampleCount"
  threshold           = "40"
  alarm_description   = "This metric checks for low CPU usage"
  alarm_actions       = [aws_autoscaling_policy.scale_in.arn]
}

# Note: Memory utilization metric is available using custom CloudWatch metrics or using the unified CloudWatch agent.

# Auto Scaling Policies for scale in and out
resource "aws_autoscaling_policy" "scale_out" {
  name                   = "example-scale-out"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.ecs_asg.id
}

resource "aws_autoscaling_policy" "scale_in" {
  name                   = "example-scale-in"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = aws_autoscaling_group.ecs_asg.id
}
