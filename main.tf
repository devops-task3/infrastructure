terraform {
  backend "s3" {
    ## variables not allowed !! 
    region = "eu-north-1"
    bucket = "devops-task-bucket-10262023"
    key    = "dev/terraform.tfstate"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.7.0"
    }
  }
}


# uncomment if need - this vars are defined in .aws
# provider "aws" {
#   region = var.AWS_REGION  
#   access_key = var.AWS_ACCESS_KEY_ID
#   secret_key = var.AWS_SECRET_ACCESS_KEY
# }

provider "aws" {
  region = "eu-north-1"
}








