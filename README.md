Application: Deploy a Rails application on AWS Elastic Container Service

Application consists of 3 workloads:
- Rails web server accepting incoming HTTP requests and connecting to Redis, 
Postgresql, Elasticsearch, AWS S3
- Sidekiq asynchronous job processor connecting to Redis, Postgresql, 
Elasticsearch, AWS S3
- DB migration task that runs only once after a new version of the application is 
deployed, running only a single script, connecting to Postgresql

A load balancer should be set up for the web server. SSL termination should be 
configured for all incoming HTTP requests.

Autoscaling based on CPU and memory usage should be set up for both the web server 
and the job processor. It should also be possible to reconfigure it later.

Set up an AWS IAM role with read and write access to the AWS S3 bucket.

Logging to AWS CloudWatch should be set up for all workloads, load balancer and 
autoscaling operations.

#### =========================
This is just brief description of how webserver configuration will looks
not implemented:
- ECS, DB migration task
#### =========================

- How to deploy the infrastructure from scratch?
  - download repo "git clone git@gitlab.com:devops-task3/infrastructure.git"
  - run: 
    terraform init
    terraform plan
    terraform apply

- How to deploy a new version of the Docker image?
  - build new version, put image into ECR
  - redeploy terraform to apply new ecs task

- Where to find logs for each workload
  - check chouldwatch logs

- How to list and update ENV variables that are set for each workload